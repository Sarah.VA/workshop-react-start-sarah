import React from 'react';
import { render, screen } from '@testing-library/react';
import HelloWorld from './index';

test('renders "Hello World"', () => {
  render(<HelloWorld />);
  const title = screen.getByText("Hello World");
  expect(title).toBeInTheDocument();
});
